package br.com.klinc.backend.infrastructure.out.database.user;

import br.com.klinc.backend.domain.model.UserEntity;
import br.com.klinc.backend.domain.ports.out.database.UserDatabasePort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class UserDatabaseService implements UserDatabasePort {

  private final UserRepository userRepository;

  @Override
  public UserEntity insertUser(InsertUserDatabaseCommand command) {
    return userRepository.save(UserEntity.builder()
        .cpf(command.cpf())
        .name(command.name())
        .email(command.email())
        .build());
  }
}
