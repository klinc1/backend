package br.com.klinc.backend.infrastructure.in.web.wpp;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/wpp")
@RequiredArgsConstructor
public class WhatsApp {


  public static final String ACCOUNT_SID = "AC196a772fa33adf7ad911008037080139";
  public static final String AUTH_TOKEN = "f3b8c90d1729eef6cb8a85497884166c";

  @PostMapping
  public void createUser() {
    Twilio.init(ACCOUNT_SID, AUTH_TOKEN);

    Message message = Message.creator(new PhoneNumber("+15558675309"),
        new PhoneNumber("+15017250604"),
        "This is the ship that made the Kessel Run in fourteen parsecs?").create();

    System.out.println(message.getSid());
  }

}
