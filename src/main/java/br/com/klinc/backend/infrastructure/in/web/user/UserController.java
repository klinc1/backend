package br.com.klinc.backend.infrastructure.in.web.user;

import br.com.klinc.backend.domain.model.UserEntity;
import br.com.klinc.backend.domain.ports.in.user.CreateUserPort;
import br.com.klinc.backend.domain.ports.in.user.CreateUserPort.CreateUserCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/user")
@RequiredArgsConstructor
public class UserController {


  private final CreateUserPort createUserPort;

  @PostMapping
  public UserEntity createUser(@RequestBody CreateUserCommand command) {
    return createUserPort.call(command);
  }

}
