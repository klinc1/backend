package br.com.klinc.backend.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.hibernate.annotations.CreationTimestamp;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class UserEntity {

  @Id
  @Column(unique = true, updatable = false, nullable = false)
  private String cpf;

  private String name;
  private String email;

  @CreationTimestamp
  @JsonIgnore
  private LocalDateTime createdAt;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) {
      return false;
    }
    UserEntity user = (UserEntity) o;
    return cpf != null && Objects.equals(cpf, user.cpf);
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }
}
