package br.com.klinc.backend.domain.usecase;


import br.com.klinc.backend.domain.model.UserEntity;
import br.com.klinc.backend.domain.ports.in.user.CreateUserPort;
import br.com.klinc.backend.domain.ports.out.database.UserDatabasePort;
import br.com.klinc.backend.domain.ports.out.database.UserDatabasePort.InsertUserDatabaseCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CreateUserUS implements CreateUserPort {

  private final UserDatabasePort userDatabase;

  @Override
  public UserEntity call(CreateUserCommand command) {
    var insertCommand = new InsertUserDatabaseCommand(command.name(), command.cpf(), command.email());

    return userDatabase.insertUser(insertCommand);
  }
}
