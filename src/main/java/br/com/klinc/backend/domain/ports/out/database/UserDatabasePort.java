package br.com.klinc.backend.domain.ports.out.database;

import br.com.klinc.backend.domain.model.UserEntity;

public interface UserDatabasePort {

  UserEntity insertUser(InsertUserDatabaseCommand command);

  record InsertUserDatabaseCommand(String name, String cpf, String email) {

  }

}
