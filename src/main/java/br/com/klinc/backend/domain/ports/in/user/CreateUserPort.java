package br.com.klinc.backend.domain.ports.in.user;

import br.com.klinc.backend.domain.model.UserEntity;

public interface CreateUserPort {

  UserEntity call(CreateUserCommand command);

  record CreateUserCommand(String name, String cpf, String email) {

  }
}
